
<section>
	<div class="wrapper">
		<div class="blogEntry clearfix">
			<div class=" column large-9">
				<?php while (have_posts()) : the_post(); ?>
					<article class="content  large-10 large-offset-1" id="article">

						<div class="blogEntry_title">
							<span><?php the_title(); ?></span>
						</div>

						<?php if (has_post_thumbnail( $post->ID ) ) : $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
							<img src="<?= $image[0]; ?>" alt="">
						<?php endif; ?>

						<?php the_content(); ?>

						<div class="blogEntry_date">
							<span>
								<?php the_date('d.m.Y');?>
							</span>
						</div>
					</article>
					<?php $id=get_the_ID();?>
				<?php endwhile; ?>
			</div>

			<div class="column large-3">
				<div class="sidebar" id="aside1">
					<div class="contactPage__phone">
						<p class="over"><?php the_field('one_news_title1',pll_current_language('slug'));  ?></p>
						<?php $phone= get_field('one_news_link1',pll_current_language('slug'));  ?>
						<a href="<?= $phone['url']; ?>"><?= $phone['title']; ?></a>
					</div>
					<div >
						<div class="office-item">
							<div class="office-item__title">
								<span><?php the_field('one_news_title2',pll_current_language('slug'));  ?></span>
								<?php $phone= get_field('one_news_link2',pll_current_language('slug'));  ?>
								<a href="<?= $phone['url']; ?>"><?= $phone['title']; ?></a>
							</div>
							<div class="office-item__work-time">
								<ul>
									<?php 
									$field=get_field('one_news_text',pll_current_language('slug'));
									$lines = explode("\n", $field);
									if ( !empty($lines) ) {
										foreach ( $lines as $line ) {
											echo '<li>'. trim( $line ) .'</li>';
										}
									}
									?>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>

	</div>
</section>

<section class="" id="blogEntry-news">
	<div class="wrapper">
		<h2 class="info-title over"><?php the_field('one_news_title3',pll_current_language('slug'));  ?></h2>
		<div class="about-news">

			<div class="about-news_slider">

				<?php $args = array( 
					'posts_per_page' 	=> 10,
					);
					$posts = get_posts( $args ); ?>
					<?php
					foreach ( $posts as $post ) : setup_postdata( $post ); ?>

					<article>
						<a href="<?php the_permalink();?>">
							<div class="about-news-item">
								<div class="about-news-item__img">
									<?php 
									if (has_post_thumbnail( $post->ID ) ) {
										$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); 
										$src=$image[0]; 
									}
									else {
										$src=get_template_directory_uri().'/dist/images/noimage.png'; 
									}
									?>

									<img src="<?= $src;?>" alt="">
								</div>

								<div class="about-news-item__title">
									<span>
										<?php the_title();?>
									</span> 	
								</div>
							</div>
						</a>

					</article>
				<?php endforeach; 
				wp_reset_postdata();?>

			</div>
		</div>
		
	</div>
</section>


<script type="text/javascript">
	  jQuery(document).ready(function() {
	  		
            jQuery(".about-news_slider").slick({
              slidesToShow:3,
              infinite: true,
              arrows:true,
              prevArrow: '<img class=" n-slide-prev" src="<?php echo get_template_directory_uri();?>/dist/images/n-slide-prev.svg" alt="">',
              nextArrow: '<img class=" n-slide-next" src="<?php echo get_template_directory_uri();?>/dist/images/n-slide-next.svg" alt="">',
              responsive: [
              {
                breakpoint: 1024,
                settings: {
                  slidesToShow: 2
                }
              },
              {
                breakpoint: 640,
                settings: {
                  slidesToShow: 1,
                  adaptiveHeight: true
                }
              }
              ]
            });

	  });


	</script>

<script>	

	jQuery(document).ready(function() {


		var a = document.querySelector('#aside1'), b = null, P = 0;
		window.addEventListener('scroll', Ascroll, false);
		document.body.addEventListener('scroll', Ascroll, false);
		function Ascroll() {
			if (b == null) {
				var Sa = getComputedStyle(a, ''), s = '';
				for (var i = 0; i < Sa.length; i++) {
					if (Sa[i].indexOf('overflow') == 0 || Sa[i].indexOf('padding') == 0 || Sa[i].indexOf('border') == 0 || Sa[i].indexOf('outline') == 0 || Sa[i].indexOf('box-shadow') == 0 || Sa[i].indexOf('background') == 0) {
						s += Sa[i] + ': ' +Sa.getPropertyValue(Sa[i]) + '; '
					}
				}
				b = document.createElement('div');
				b.style.cssText = s + ' box-sizing: border-box; width: ' + a.offsetWidth + 'px;';
				a.insertBefore(b, a.firstChild);
				var l = a.childNodes.length;
				for (var i = 1; i < l; i++) {
					b.appendChild(a.childNodes[1]);
				}
				a.style.height = b.getBoundingClientRect().height + 'px';
				a.style.padding = '0';
				a.style.border = '0';
			}
			var Ra = a.getBoundingClientRect(),
			R = Math.round(Ra.top + b.getBoundingClientRect().height - document.querySelector('#article').getBoundingClientRect().bottom);  // селектор блока, при достижении нижнего края которого нужно открепить прилипающий элемент
			if ((Ra.top - P) <= 0) {
				if ((Ra.top - P) <= R) {
					b.className = 'stop';
					b.style.top = - R +'px';
				} else {
					b.className = 'sticky';
					b.style.top = P + 'px';
				}
			} else {
				b.className = '';
				b.style.top = '';
			}
			window.addEventListener('resize', function() {
				a.children[0].style.width = getComputedStyle(a, '').width
			}, false);
		}
	})
</script>
