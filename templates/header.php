<header>


  <section class="sub-header">
    <div class="main-logo columns large-4">
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
        <img src="<?php echo get_template_directory_uri();?>/dist/images/main-logo4.svg" alt="">
      </a>
    </div>
    <div class="logo2 columns large-4 show-for-small-only">
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
        <img src="<?php echo get_template_directory_uri();?>/dist/images/_logo.svg" alt="">
      </a>
    </div>
    <div class="logo2 columns large-4 show-for-large">
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
        <img src="<?php echo get_template_directory_uri();?>/dist/images/logo_3.svg" alt="">
      </a>
    </div>
    <div class="info-group columns large-4 right hide-for-small-only">
      <div class="logo3 ">
        <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
          <img src="<?php echo get_template_directory_uri();?>/dist/images/_logo.svg" alt="">
        </a>
      </div>
      <!-- <div class="sub-phone"><a  href="tel:+38 050-848-15-58">+38 050-848-15-58</a><span class="show-for-large"><img src="/dist/images/viber-ic.svg" alt=""></span>
      </div> -->
      <div class="lang-switcher">
       <select id="lang-select" onchange="if (this.value) window.location.href=this.value">
        <option value=""><?php echo pll_current_language('name');?></option>
        <?php
        $translations = pll_the_languages(array('raw'=>1,'hide_current'=>1));
        foreach ($translations as $translation) {
          echo '<option value="'.$translation['url'].'">'.$translation['name'].'</option>';
        }
        ?>
      </select>

    </div>

  </div>
</section>


<nav class="navigation">

 <div class="mob_open show-for-small-only">
  <div class="icon ">
    <div class="burger"></div>
  </div>
</div>

<a href="<?= $phone['url']?>" class="show-for-small-only"><?= $phone['title']?></a>

<div class="menu-wrap">
  <ul class=" menu"  id="menu" >
    <?php if (has_nav_menu('primary_navigation')) :?>
      <?php wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav', 'container' => '', 'items_wrap' => '%3$s', 'walker' => new Roots\Sage\Extras\Foundation_Nav_Menu()]);?>
    <?php endif;?>
  </ul>

  <ul id="mob-lang" class="show-for-small-only">
    <?php Roots\Sage\Extras\custom_ppl_languages_with_active(); ?>
    <!--
      <li class="active">УКР</li>
      <li>РУС</li> -->
    </ul>

  </div>


</nav>
</header>
