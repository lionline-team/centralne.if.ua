
<section>
	<div class="wrapper">
		<div class="last-news clearfix">
			<div class="info-title over">
				<span><?php _e('новини','lionline');?></span>
			</div>

			<?php $posts = get_posts( ); ?>
			<?php setup_postdata( $post ); ?>
			<div class="last-news__info column large-5">
				<div class="last-news-title">
					<span><?php the_title();?></span>
				</div>
				<div class="last-news-text hide-for-small-only">
					<p>
						<?php echo get_the_excerpt();?>
					</p>
				</div>
				<div class="last-news-btn">
					<a href="<?php the_permalink();?>" class="batton"><?php _e('читати далі','lionline');?></a>
				</div>
			</div>
      <?php wp_reset_postdata(); ?>
			<div class="last-news__img column large-6">
				<?php
				if (has_post_thumbnail( $post->ID ) ) {
					$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
					$src=$image[0];
				}
				else {
					$src=get_template_directory_uri().'/dist/images/noimage.png';
				}
				?>
				<img src="<?= $src;?>" alt="" class="large-10">
			</div>
		</div>
	</div>
</section>

<section class="all-news">
	<div class="wrapper">
		<div class="news-items clearfix ">
			<?php while (have_posts()): the_post(); ?>
				<?php $thepost=get_the_ID();?>
        <?php if (!$thepost) continue; ?>
				<article class="column large-4 medium-6 small-12">
					<a href="<?php the_permalink();?>">
						<div class="blog-item">
							<div class="blog-item__img">
								<?php if (has_post_thumbnail( ) ) : $image = wp_get_attachment_image_src( get_post_thumbnail_id(  ), 'single-post-thumbnail' ); ?>
									<img src="<?= $image[0]; ?>" alt="">
								<?php endif; ?>
							</div>
							<div class="blog-item__text">
								<div class="blog-item-title">
									<span><?php the_title();?></span>
								</div>
								<div class="blog-item-date">
									<span><?php the_date('d.m.Y');?></span>
								</div>
							</div>
						</div>
					</a>
				</article>

			<?php endwhile; ?>


		</div>
	</div>
</section>
