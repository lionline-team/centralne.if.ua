<?php
/**
 * Template Name: AboutUs Template
 */
?>

<?php while (have_posts()) : the_post(); ?>
	<section class="aboutPage_about" >
		<div class="wrapper">
			<div class="aboutPage clearfix">
				<div class="aboutPage__content column large-7">
					<div class="info-title over">
						<span><?php the_field('block1_title');?></span>
					</div>
					<div class="aboutPage-text large-11">
						<?php the_field('block1_text');?>
					</div>


					<?php if( have_rows('developer_stats',pll_current_language('slug')) ):?>
						<div class="aboutPage_advantages">
							<?php while ( have_rows('developer_stats',pll_current_language('slug')) ) : ?>
								<?php the_row(); ?>
								<div class="advantages_item">
									<span><?php the_sub_field('metric');?></span>
									<p><?php the_sub_field('text');?></p>
								</div>
							<?php  endwhile; ?>
						</div>
					<?php endif; ?>
				</div>
				<div class="aboutPage__img column large-5">
					<img src="<?php the_field('block1_image');?>" alt="">
				</div>
			</div>

		</div>
		<div class="comfort-block-wrap">
			<img src="<?php echo get_template_directory_uri();?>/dist/images/comf-bg.svg" alt="">
		</div>
	</section>


	<section>
		<div class="wrapper">
			<div class="about-work clearfix">
				<div class="about-work__img column large-5">
					<img src="<?php the_field('block2_image');?>" alt="">
				</div>
				<div class="about-work__content column large-6">
					<div class="info-title over">
						<span><?php the_field('block2_title');?></span>

					</div>
					<?php the_field('block2_text');?>
				</div>
			</div>
		</div>

	</section>
	<section id="top-block" >
		<div class="wrapper">
			<div class=" top-block" clearfix">

				<div class="top-block-content clearfix ">
					<div class="column large-6 medium-6">
						<div class="info-title over">
							<span><?php the_field('block3_title');?></span>
						</div>
						<?php the_field('block3_text');?>
						<div class="top-block__btn">
							<?php $button=get_field('block3_button'); ?>
							<?php if ($button) : ?>
								<a href="<?= $button['url'];?>" class="batton"><?= $button['title'];?></a>
							<?php endif; ?>
						</div>
					</div>

					<div class="top-block__img column large-5 medium-6">
						<img src="<?php the_field('block3_image');?>" alt="">
					</div>
				</div>

			</div>
		</div>

		<div class="top-block-wrap">
			<img src="<?php echo get_template_directory_uri();?>/dist/images/comf-bg.svg" alt="">
		</div>
	</section>

	<section class="" id="about-news">
		<div class="wrapper">
			<h2 class="info-title over"><?php the_field('objects_title');?></h2>
			<div class="about-news">
				<div class="about-news_slider">
					<?php if( have_rows('objects')):?>
						<?php while ( have_rows('objects') ) : ?>
							<?php the_row(); ?>
							<article>
								
								<div class="about-news-item">
									<div class="about-news-item__img">
										<img src="<?php the_sub_field('photo');?>" alt="">
									</div>

									<div class="about-news-item__title">
										<span>
											<?php the_sub_field('title');?>
										</span> 	
									</div>
								</div>
						
							</article>
						<?php  endwhile; ?>
					<?php endif; ?>

				</div>

			</div>

		</div>
	</section>

    <script type="text/javascript">
	  jQuery(document).ready(function() {
	  		
            jQuery(".about-news_slider").slick({
              slidesToShow:3,
              infinite: true,
              arrows:true,
              prevArrow: '<img class=" n-slide-prev" src="<?php echo get_template_directory_uri();?>/dist/images/n-slide-prev.svg" alt="">',
              nextArrow: '<img class=" n-slide-next" src="<?php echo get_template_directory_uri();?>/dist/images/n-slide-next.svg" alt="">',
              responsive: [
              {
                breakpoint: 1024,
                settings: {
                  slidesToShow: 2
                }
              },
              {
                breakpoint: 640,
                settings: {
                  slidesToShow: 1,
                  adaptiveHeight: true
                }
              }
              ]
            });

	  });


	</script>
<?php endwhile; ?>
