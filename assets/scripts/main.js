/* ========================================================================
* DOM-based Routing
* Based on http://goo.gl/EUTi53 by Paul Irish
*
* Only fires on body classes that match. If a body class contains a dash,
* replace the dash with an underscore when adding it to the object below.
*
* .noConflict()
* The routing is enclosed within an anonymous function so that you can
* always reference jQuery with $, even when in .noConflict() mode.
* ======================================================================== */

(function($) {

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    'common': {
      init: function() {
        // JavaScript to be fired on all pages

        $(document).foundation(); // Foundation JavaScript

      },
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired
      }
    },
    // Home page
    'home': {
      init: function() {
        // JavaScript to be fired on the home page
      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },
    // About us page, note the change from about-us to about_us.
    'about_us': {
      init: function() {
        // JavaScript to be fired on the about us page
      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.





// ====================== smooth scroll anchor ================

// jQuery(function() {
  //   jQuery('.scrolldown').click(function() {
    //     if (location.pathname.replace(/^\//,'') === this.pathname.replace(/^\//,'') && location.hostname === this.hostname) {
      //       var target = jQuery(this.hash);
      //       target = target.length ? target : jQuery('[name=' + this.hash.slice(1) +']');
      //       if (target.length) {
        //         jQuery('html,body').animate({
          //           scrollTop: target.offset().top - 80
          //         }, 1000);
          //         return false;
          //       }
          //     }
          //   });
          // });

          // ====================== end smooth scroll anchor ================



          jQuery(document).ready(function(){      // global document.ready











            // jQuery(".about-news_slider").slick({
            //   slidesToShow:3,
            //   infinite: true,
            //   arrows:true,
            //   prevArrow: '<img class=" n-slide-prev" src="<?php echo get_template_directory_uri();?>/dist/images/n-slide-prev.svg" alt="">',
            //   nextArrow: '<img class=" n-slide-next" src="<?php echo get_template_directory_uri();?>/dist/images/n-slide-next.svg" alt="">',
            //   responsive: [
            //   {
            //     breakpoint: 1024,
            //     settings: {
            //       slidesToShow: 2
            //     }
            //   },
            //   {
            //     breakpoint: 640,
            //     settings: {
            //       slidesToShow: 1,
            //       adaptiveHeight: true
            //     }
            //   }
            //   ]
            // });

            // jQuery(".scrol").on("click", function (event) {
            //   event.preventDefault();
            //   var id  = jQuery(this).attr('href');

            //   if (typeof id==='undefined') {
            //     var url=jQuery(this).find('a').attr('href');
            //     id=url.substring(url.indexOf('#'));
            //     console.log(id);

            //     if (jQuery(id).length > 0) {
            //       var  top = jQuery(this).offset().top -110;
            //     }
            //     else
            //     {
            //       console.log('no elemment');
            //     }

            //   }



              jQuery(".scrol").on("click", function (event) {

                var id  = jQuery(this).attr('href'),
                    top = jQuery(id).offset().top -110;

                if(id.indexOf('#')) {
                  event.preventDefault();
                }

                jQuery('body,html').animate({scrollTop: top}, 1000);
                console.log(id);

                if (id == '#cont') {
                  setTimeout(function(){
                  jQuery('input#name').focus();
                  }, 1000);
                }

                if (jQuery(this).attr('id') === 'tab-infrastructure') {
                  jQuery('#infrastructure-label').click();
                }

                if (jQuery(this).attr('id') === 'tab-about') {
                  jQuery('#about-city-label').click();
                }

                if (jQuery(this).attr('id') === 'tab-map') {
                  jQuery('#city-map-label').click();
                }

              });




              // jQuery(".scrol-about").click(function(e){
              //   e.preventDefault();
              //   var top = jQuery('#about-city-label').offset().top -110;
              //   console.log(top)
              //   jQuery('body,html').animate({scrollTop: top}, 1000);
              //   jQuery('#about-city-label').click();
              // });

              // jQuery(".scrol-infrastructure").click(function(e){
              //   e.preventDefault();
              //   var top = jQuery('#infrastructure-label').offset().top -110;
              //   jQuery('body,html').animate({scrollTop: top}, 1000);
              //   jQuery('#infrastructure-label').click();
              // });

              // jQuery(".scrol-map").click(function(e){
              //   e.preventDefault();
              //   var top = jQuery('#city-map-label').offset().top -110;
              //   jQuery('body,html').animate({scrollTop: top}, 1000);
              //   jQuery('#city-map-label').click();
              // });


            //   jQuery('body,html').animate({scrollTop: top}, 1000);
            //   // console.log(id);
            //   if (id == '#cont') {
            //     setTimeout(function(){
            //      jQuery('input#name').focus();
            //    }, 1000);

            //   }
            //   if (jQuery(this).attr('id') === 'tab-infrastructure') {

            //     jQuery('#infrastructure-label').click();

            //   }
            //   if (jQuery(this).attr('id') === 'tab-about') {

            //     jQuery('#about-city-label').click();
            //   }
            //   if (jQuery(this).attr('id') === 'tab-map') {

            //     jQuery('#city-map-label').click();
            //   }
            // });

            // jQuery('#tab-infrastructure').click(function(){

              //   jQuery('#infrastructure-label').click();
              // })

              jQuery("#lang-select").niceSelect();


              jQuery( ".icon" ).click(function(){
                jQuery(this).toggleClass('active');
                jQuery('.menu-wrap').toggleClass('active');
                jQuery('body').removeClass('hiddenScroll');
              });

               jQuery( ".menu li" ).click(function(){

                jQuery('.menu-wrap').toggleClass('active');
                jQuery('body').removeClass('hiddenScroll');
              });


              jQuery(document).mouseup(function (e) {
                var container = jQuery(".navigation");
                if (container.has(e.target).length === 0){
                 jQuery('.menu-wrap').removeClass('active');
                 jQuery( ".icon" ).removeClass('active');
                 jQuery('body').removeClass('hiddenScroll');
               }
             });



              /////////////   form success      ////////////


              jQuery(".contact-fom").on('submit',function(e){
                jQuery(this).find('button').addClass('animate');
                e.preventDefault();
                jQuery(this).trigger("reset");
                setTimeout(function(){
                  jQuery('.button').removeClass('animate');
                  jQuery(".contact_us__success").addClass('active');
                },1000);
                setTimeout(function(){
                  jQuery(".contact_us__success").removeClass('active');
                },6000);
              });
              jQuery("#btn_success").click(function(){
                jQuery(".contact_us__success").removeClass('active');
              })



              ///////////////////////////////////////////////////////////

              // jQuery("#sections_show").click(function(e){
                //   e.preventDefault();
                //   jQuery(".sections-items ").toggleClass("active");
                //   // jQuery(this).hide();
                // })

                function autoHeightAnimate(element) {
                  var curHeight = element.height(), // Get Default Height
                  autoHeight = element.css('height', 'auto').height(); // Get Auto Height
                  element.height(curHeight); // Reset to Default Height
                  element.stop().animate({
                    height: autoHeight
                  }); // Animate to Auto Height
                }

                jQuery('.sections_show').click(function(e) {
                  e.preventDefault();
                  jQuery(".sections-item").show();
                  autoHeightAnimate(jQuery(".sections-items "));
                  jQuery(this).hide();

                });
                autoHeightAnimate(jQuery(".sections-items "));

                /* Function to animate height: auto */



                jQuery().fancybox({
                  selector : '[data-fancybox="images"]',
                  thumbs  : {
                   // autoStart : true

                 },
                 loop:true,
                 buttons : [
                 // 'slideShow',
                 // 'fullScreen',
                 // 'thumbs',
                 'close'
                 ],
                 // toolbar  : false,
                 // smallBtn : false,

               });

              });

    jQuery('[data-triger]').click(function(){
      var collection =  jQuery(this).data('collection');
      var triger = jQuery(this).data('triger');

      jQuery('[data-collection="'+collection+'"][data-triger]').removeClass('tab-active');

      jQuery(this).addClass('tab-active');

      jQuery('[data-collection="'+collection+'"]').removeClass('active');
      jQuery('[data-collection="'+collection+'"][data-target="'+triger+'"]').addClass('active');
    });

////////////////////////////////////////////////////


jQuery(document).ready(function(){


 // wordpress admin panel
 jQuery('html').attr('style', 'margin-top: 0!important');
 jQuery('#wpadminbar').addClass('overflow');
 var hide;
 jQuery('#wpadminbar').on('mouseover', function(){
  setTimeout(function(){
    jQuery('#wpadminbar').removeClass('overflow');
  },1000);
  if(!jQuery('#wpadminbar').hasClass('open')){
    jQuery('#wpadminbar').addClass('open overflow');
  } else{
    clearTimeout(hide);
  }
});
 jQuery('#wpadminbar').on('mouseleave', function(){
  hide = setTimeout(function(){
    jQuery('#wpadminbar').addClass('overflow');
    jQuery('#wpadminbar').removeClass('open');
  },2000);
});
 // end wordpress admin panel

 setHeight();
});

jQuery( window ).resize(function() {
  setHeight();
});

function setHeight() {
  var window_height = jQuery( window ).height();
  var info_height = jQuery('.info').height();
  var build_height = jQuery('.build-proc').height();

  jQuery('.container').css('height', window_height);
  jQuery('.container > .first').css('height', info_height);
  jQuery('.build').css('height', build_height);
}





