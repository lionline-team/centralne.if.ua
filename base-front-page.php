<?php

use Roots\Sage\Setup;
use Roots\Sage\Wrapper;

?>

<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<?php get_template_part('templates/head'); ?>
<body class="index-back">
  <!--[if lte IE 9]>
  <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
  <![endif]-->

  <?php get_template_part( 'templates/header'); ?>


  <?php include Wrapper\template_path(); ?>

  <?php get_template_part( 'templates/footer'); ?>
  <?php wp_footer();?>
  <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

  <script type="text/javascript">
    jQuery(document).ready(function() {

      jQuery(".scrol-about").click(function(e){
        e.preventDefault();
        var top = jQuery('#about-city-label').offset().top -110;
        // console.log(top)
        jQuery('body,html').animate({scrollTop: top}, 1000);
        jQuery('#about-city-label').click();
      });

      jQuery(".scrol-infrastructure").click(function(e){
        e.preventDefault();
        var top = jQuery('#infrastructure-label').offset().top -110;
        jQuery('body,html').animate({scrollTop: top}, 1000);
        jQuery('#infrastructure-label').click();
      });

      jQuery(".scrol-map").click(function(e){
        e.preventDefault();
        var top = jQuery('#city-map-label').offset().top -110;
        jQuery('body,html').animate({scrollTop: top}, 1000);
        jQuery('#city-map-label').click();
      });

      jQuery(".scrol-apartments").click(function(e){
        // e.preventDefault();
        var top = jQuery('#apr').offset().top -110;
        jQuery('body,html').animate({scrollTop: top}, 1000);

      });


      jQuery(".slider_bottom").click(function() {
        jQuery('html, body').animate({
          scrollTop: jQuery("#tab-info").offset().top
        }, 1000);
      });
    });
    jQuery(function() {

      jQuery(".main-slider").slick({
        prevArrow: '<img class="arrow arrow_left" src="<?php echo get_template_directory_uri();?>/dist/images/arrow_left.svg" alt="">',
        nextArrow: '<img class="arrow arrow_right" src="<?php echo get_template_directory_uri();?>/dist/images/arrow_right.svg" alt="">',
        infinite: true,
        dots: true,
        speed: 1000,
        autoplay: true,
        autoplaySpeed: 10000,
      });


      var carousel = jQuery(".carousel-news");
      var slider;

      carousel.slick({

        prevArrow: '<img class=" n-slide-prev" src="<?php echo get_template_directory_uri();?>/dist/images/n-slide-prev.svg" alt="">',
        nextArrow: '<img class=" n-slide-next" src="<?php echo get_template_directory_uri();?>/dist/images/n-slide-next.svg" alt="">',
        arrows: true,
        slidesToShow:3,
        responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 2
          }
        },
        {
          breakpoint: 640,
          settings: {
            slidesToShow: 1,
            adaptiveHeight: true
          }
        }
        ]
      });

      jQuery('[href="#infrastructure"]').click(function() {

        carousel.resize();
      });

      var count_news = carousel.slick( "getSlick" );

      slider = jQuery( ".slider" ).slider({
        min : 0,
        max : 8,
        slide: function(event, ui) {
          var slick = carousel.slick( "getSlick" );
          goTo = ui.value * (slick.slideCount-1) / 8;
          carousel.slick( "goTo", goTo );
        }
      });
    });



    if (window.location.hash.length>1)
    {
      setTimeout(function(){ 
        jQuery(window.location.hash).trigger('click');

      }, 1);

    } 


  </script>

</body>
</html>

