<?php

namespace Roots\Sage\Extras;

use Roots\Sage\Setup;

/**
 * Add <body> classes
 */
function body_class($classes) {
  // Add page slug if it doesn't exist
  if (is_single() || is_page() && !is_front_page()) {
    if (!in_array(basename(get_permalink()), $classes)) {
      $classes[] = basename(get_permalink());
    }
  }

  // Add class if sidebar is active
  if (Setup\display_sidebar()) {
    $classes[] = 'sidebar-primary';
  }

  return $classes;
}
add_filter('body_class', __NAMESPACE__ . '\\body_class');

/**
 * Clean up the_excerpt()
 */
function excerpt_more() {
  return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'sage') . '</a>';
}
add_filter('excerpt_more', __NAMESPACE__ . '\\excerpt_more');

class Foundation_Nav_Menu extends \Walker_Nav_Menu {
  function start_lvl(&$output, $depth = 0, $args = Array() ) {
    $indent = str_repeat("\t", $depth);
    $output .= "\n$indent<ul class=\"menu\">\n";
  }
}

function nav_menu_title_by_location ($location)
{
 $theme_locations = get_nav_menu_locations();
 $menu_obj = get_term( $theme_locations[$location], 'nav_menu' );
 $menu_name = $menu_obj->name;
 return $menu_name;
}

function display_menu_with_title($location)
{ ?>
  <div class="columns medium-4">
    <div class="footer-name"><?php echo \Roots\Sage\Extras\nav_menu_title_by_location($location);?></div>
    <ul>
      <?php if (has_nav_menu($location)) :?>
        <?php wp_nav_menu(['theme_location' => $location, 'menu_class' => 'nav', 'container' => '', 'items_wrap' => '%3$s', 'walker' => new Foundation_Nav_Menu()]);?>
      <?php endif;?>
    </ul>
  </div>
  <?php
}



function custom_ppl_languages_with_active() {
  $translations = pll_the_languages(array('raw'=>1));

  foreach ($translations as $translation) {

    if (pll_current_language('slug')==$translation['slug'])
      echo '<li class="active"><a href="'.$translation['url'].'">'.$translation['name'].'</a></li>';
    else
     echo '<li><a href="'.$translation['url'].'">'.$translation['name'].'</a></li>';
 }
  // var_dump($translations);
}