<?php
/**
 * Template Name: One Flat Template
 */
?>

<?php while (have_posts()) : the_post(); ?>

	<section>
		<div class="wrapper">
			<div class="apartment-item clearfix">
				<div class="apartment-item__img column medium-6 large-6">
					<a href="<?php the_field('main_image');?>" data-fancybox="images">
						<img src="<?php the_field('main_image');?>" alt="">
					</a>
				</div>
				<div class="apartment-item__info column medium-6 large-5">
					<h2 class="info-title"><?php the_title();?></h2>
					<div class="sub_title over">
						<span><?php the_field('subtitle');?></span>
						<p><?php the_field('square');?></p>
					</div>
					<div class="apartment-item__text">
						<p><?php the_field('description');?></p>
					</div>
					<div class="apartment-item__btn">
							<a class="batton scrol  " href="#cont"><?php _e('дізнатись ціни','lionline');?></a>
							<?php $phone= get_field('contact_mainphone',pll_current_language('slug'));  ?>
              <a class="batton btn-right batton_light " href="<?= $phone['url']?>" target='_blank'><?= $phone['title']?></a>
					</div>
				</div>
			</div>
		</div>
	</section>



	<?php if( have_rows('sections') ):?>
		<section>
			<div class="wrapper">
				<div class="block-sections clearfix">
					<div class="info-title">
						<span><?php _e('Містечко “Центральне”','lionline');?></span>
					</div>
					<div class="sections-items clearfix">
						<?php while ( have_rows('sections') ) : ?>
							<?php the_row(); ?>
							<article>
								<div class="column large-3 medium-4 small-6">
									<div class="sections-item ">
										<div class="sections-item__img">
											<?php $image=get_sub_field('image'); ?>
											<a data-fancybox="images" href="<?php echo $image['url'];?>">
												<img src="<?php echo $image['url'];?>" alt="">
											</a>
										</div>
										<div class="sections-item__name">
											<span><?php the_sub_field('title');?></span>
										</div>
									</div>
								</div>
							</article>
						<?php  endwhile; ?>
					</div>

					<?php $sections_count=count(get_field('sections'));?>
					<?php if ($sections_count>8) : ?>
						<div class="sections-items_hide  clearfix">
						</div>
						<div class="block-sections__btn show-for-large">
							<a href="#" class="batton sections_show batton_light"><?php _e('показати всі секції','lionline');?></a>
						</div>
					<?php endif;?>
					<?php if ($sections_count>6) : ?>
						<div class="sections-items_hide clearfix">
						</div>
						<div class="block-sections__btn show-for-medium-only">
							<a href="#"  class="batton sections_show batton_light"><?php _e('показати всі секції','lionline');?></a>
						</div>
					<?php endif;?>
					<?php if ($sections_count>4) : ?>
						<div class="sections-items_hide clearfix">
						</div>
						<div class="block-sections__btn show-for-small-only">
							<a href="#"  class="batton sections_show batton_light"><?php _e('показати всі секції','lionline');?></a>
						</div>
					<?php endif;?>
				</div>
			</div>
		</section>
	<?php endif; ?>

	<section id="comfort-block" >
		<div class="wrapper">
			<div class=" comfort-block" clearfix">
				<div class="info-title over">
					<span><?php the_field('block1_title');?></span>
				</div>
				<div class="comfort-block-content">
					<div class="column large-8">
						<ul class="comfort-list clearfix">
							<?php
							$field=get_field('block1_list');
							$lines = explode("\n", $field);
							if ( !empty($lines) ) {
								foreach ( $lines as $line ) {
									echo '<li class=" column">'. trim( $line ) .'</li>';
								}
							}
							?>
						</ul>
					</div>

					<div class="comfort-block__img column large-4">
						<img src="<?php the_field('block1_image');?>" alt="">
					</div>
				</div>

			</div>
		</div>

		<div class="comfort-block-wrap">
			<img src="<?php echo get_template_directory_uri();?>/dist/images/comf-bg.svg" alt="">
		</div>
	</section>


	<section>
		<div class="wrapper">
			<div class="town-block clearfix">
				<div class="town-block__img column large-6">
					<img src="<?php the_field('block2_image');?>" alt="">
				</div>
				<div class="town-block__content column large-5">
					<div class="info-title">
						<span><?php the_field('block2_title');?></span>

					</div>
					<ul>
						<?php
						$field=get_field('block2_list');
						$lines = explode("\n", $field);
						if ( !empty($lines) ) {
							foreach ( $lines as $line ) {
								echo '<li class=" ">'. trim( $line ) .'</li>';
							}
						}
						?>
					</ul>
					<div class="town-block__btn">
						<a class="batton scrol" href="#cont"><?php _e('дізнатись ціни','lionline');?></a>
					</div>
				</div>
			</div>
		</div>

	</section>

	<?php get_template_part( 'templates/block', 'tabs');?>

<?php endwhile; ?>
