<?php

namespace Roots\Sage\Setup;

use Roots\Sage\Assets;

/**
 * Theme setup
 */
function setup() {
  // Enable features from Soil when plugin is activated
  // https://roots.io/plugins/soil/
  add_theme_support('soil-clean-up');
  add_theme_support('soil-nav-walker');
  add_theme_support('soil-nice-search');
  add_theme_support('soil-jquery-cdn');
  add_theme_support('soil-relative-urls');

  // Make theme available for translation
  // Community translations can be found at https://github.com/roots/sage-translations
  load_theme_textdomain('lionline', get_template_directory() . '/lang');

  // Enable plugins to manage the document title
  // http://codex.wordpress.org/Function_Reference/add_theme_support#Title_Tag
  add_theme_support('title-tag');

  // Register wp_nav_menu() menus
  // http://codex.wordpress.org/Function_Reference/register_nav_menus
  register_nav_menus([
    'primary_navigation' => __('Primary Navigation', 'lionline'),
    'footer1_navigation' => __('Footer Column1 Navigation', 'lionline'),
    'footer2_navigation' => __('Footer Column2 Navigation', 'lionline'),
    'footer3_navigation' => __('Footer Column3 Navigation', 'lionline'),
    'footer4_navigation' => __('Footer Column4 Navigation', 'lionline'),
    'footer5_navigation' => __('Footer Column5 Navigation', 'lionline'),
  ]);

  // Enable post thumbnails
  // http://codex.wordpress.org/Post_Thumbnails
  // http://codex.wordpress.org/Function_Reference/set_post_thumbnail_size
  // http://codex.wordpress.org/Function_Reference/add_image_size
  add_theme_support('post-thumbnails');

  // Enable post formats
  // http://codex.wordpress.org/Post_Formats
  add_theme_support('post-formats', ['aside', 'gallery', 'link', 'image', 'quote', 'video', 'audio']);

  // Enable HTML5 markup support
  // http://codex.wordpress.org/Function_Reference/add_theme_support#HTML5
  add_theme_support('html5', ['caption', 'comment-form', 'comment-list', 'gallery', 'search-form']);

  // Use main stylesheet for visual editor
  // To add custom styles edit /assets/styles/layouts/_tinymce.scss
  add_editor_style(Assets\asset_path('styles/main.css'));
}
add_action('after_setup_theme', __NAMESPACE__ . '\\setup');

/**
 * Register sidebars
 */
function widgets_init() {
  register_sidebar([
    'name'          => __('Primary', 'sage'),
    'id'            => 'sidebar-primary',
    'before_widget' => '<section class="widget %1$s %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h3>',
    'after_title'   => '</h3>'
  ]);

  register_sidebar([
    'name'          => __('Footer', 'sage'),
    'id'            => 'sidebar-footer',
    'before_widget' => '<section class="widget %1$s %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h3>',
    'after_title'   => '</h3>'
  ]);
}
add_action('widgets_init', __NAMESPACE__ . '\\widgets_init');

/**
 * Determine which pages should NOT display the sidebar
 */
function display_sidebar() {
  static $display;

  isset($display) || $display = !in_array(true, [
    // The sidebar will NOT be displayed if ANY of the following return true.
    // @link https://codex.wordpress.org/Conditional_Tags
    is_404(),
    is_front_page(),
    is_page_template('template-custom.php'),
  ]);

  return apply_filters('sage/display_sidebar', $display);
}

/**
 * Theme assets
 */
function assets() {
  wp_enqueue_style('sage/css', Assets\asset_path('styles/main.css'), false, null);

  if (is_single() && comments_open() && get_option('thread_comments')) {
    wp_enqueue_script('comment-reply');
  }

  wp_enqueue_script('sage/js', Assets\asset_path('scripts/main.js'), ['jquery'], null, true);
}
add_action('wp_enqueue_scripts', __NAMESPACE__ . '\\assets', 100);


$languages = array( 'uk', 'ru' );
foreach ( $languages as $lang ) {
  acf_add_options_sub_page( array(
    'page_title' => 'Options (' . strtoupper( $lang ) . ')',
    'menu_title' => __('Options (' . strtoupper( $lang ) . ')', 'lionline'),
    'menu_slug'  => "options-${lang}",
    'post_id'    => $lang,
    'parent'     => $parent['menu_slug']
  ) );
}


add_filter('acf/settings/google_api_key', function () {
  return 'AIzaSyApj8qSzyO4DqXeQtFQPKI4mi6p-MBwlpo';
});


function send_message() {

  ini_set('display_errors',1); //show all php errors
  error_reporting(E_ALL);


  if ( isset($_REQUEST) ) {
    $link =$_REQUEST['link'];
    $name =$_REQUEST['name'];
    $phone=$_REQUEST['phone'];
  }

  $mail=array(
    'filbud.if@gmail.com',
    'fil-bud@i.ua',
    get_option('admin_email')
  );
  // var_dump($mail);


  $date=date('d-m-Y');
  $title="Нове повідомлення (".$date.')';
  $body="В вас новий запит від:".$name  . "<br>";
  $body.="Tel: ".$phone."<br>";
  $body.="Відправлено з сторінки: <a href='".get_permalink( $link )."'>".get_the_title( $link )."</a><br>";

  // var_dump($body);


  $headers = array(
    'Content-Type: text/html; charset=UTF-8',
    "From: ".get_option('siteurl')." <".$mail[0].">" . "\r\n");
  $sent_message = wp_mail( $mail, $title, $body, $headers );
  if ( $sent_message ) {
    // the message was sent...
    echo 'Повідомлення адміністратору відправлено ('.$name.')';
  } else {
    // the message was not sent...
    var_dump($_REQUEST);
    echo 'Повідомлення адміністратору не відправлено!</br>';
  }
  die();
}
add_action( 'wp_ajax_nopriv_sendmsg', __NAMESPACE__ . '\\send_message' );
add_action( 'wp_ajax_sendmsg', __NAMESPACE__ . '\\send_message' );


add_action('wp_head',__NAMESPACE__ . '\\pluginname_ajaxurl');
function pluginname_ajaxurl() {
  echo '<script type="text/javascript">var ajaxurl =\''.admin_url('admin-ajax.php').'\'</script>';
}

// add editor the privilege to edit theme
$role_object = get_role( 'editor' );
$role_object->add_cap( 'edit_theme_options' );
