
<section class="tab-info" id="tab-info" >
	<ul class="tabs" data-tabs id="city-info">
		<li class="tabs-title is-active"><a href="#about-city" aria-selected="true"><?php _e('Про містечко','lionline');?></a></li>
		<li class="tabs-title"><a href="#infrastructure"><?php _e('Інфраструктура','lionline');?></a></li>
		<li class="tabs-title"><a href="#city-map"><?php _e('Розташування','lionline');?></a></li>
	</ul>

	<div class="tabs-content" data-tabs-content="city-info">
		<div class="tabs-panel is-active" id="about-city">
			<div class="row">
				<div class="columns medium-12 large-6 small-12">
					<div class="city-image">
						<img src="<?php the_field('about_image',pll_current_language('slug'));  ?>" alt="">
					</div>
				</div>
				<div class="columns medium-12 large-6 small-12 city-descr">
					<h2 class="info-title"><?php the_field('about_title',pll_current_language('slug'));  ?></h2>
					<?php the_field('about_text',pll_current_language('slug'));  ?>
				</div>
			</div>
		</div>
		<div class="tabs-panel" id="infrastructure">
			<section class="tab-slider">

				<?php if( have_rows('infra_items',pll_current_language('slug')) ):?>
					<?php while ( have_rows('infra_items',pll_current_language('slug')) ) : ?>
						<?php the_row(); ?>

						<div class="infra-slide">
							<div class="infra-wrap">
								<img src="<?php the_sub_field('icon');?>" alt="">
								<p><?php the_sub_field('text');?></p>
							</div>
						</div>
					<?php  endwhile; ?>
				<?php endif; ?>

			</section>
			<!-- <div class="image-back">
			<img src="<?php echo get_template_directory_uri();?>/dist/images/tabs-img.png" alt="">
			<div class="img-wrap"></div>
		</div> -->

	</div>
	<div class="tabs-panel" id="city-map">
		<div id="map">
			<script  defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyApj8qSzyO4DqXeQtFQPKI4mi6p-MBwlpo&amp;callback=initMap"></script>
		</div>
	</div>
</div>
</div>
</section>


<script type="text/javascript">
	jQuery(document).ready(function() {

		var tabsSlider =  jQuery(".tab-slider").slick({
			prevArrow: '<img class="arrow arrow_left" src=<?php echo get_template_directory_uri();?>/dist/images/arrow_left_yellow.svg" alt="">',
			nextArrow: '<img class="arrow arrow_right" src=<?php echo get_template_directory_uri();?>/dist/images/arrow_right_yellow.svg" alt="">',
			slidesToShow:4,
			dots: false,
			speed: 1000,
			// autoplay: true,
			autoplaySpeed: 5000,
			responsive: [
			{
				breakpoint: 1024,
				settings: {
					slidesToShow: 3
				}
			},
			{
				breakpoint: 640,
				settings: {
					slidesToShow: 1
				}
			}
			]
		});

		jQuery(".tabs").on("change.zf.tabs", function (event, tab) {
			tabsSlider.slick("setPosition");
		});


		var carousel = jQuery(".carousel-news");
		var slider;

		carousel.slick({

			prevArrow: '<img class=" n-slide-prev" src=<?php echo get_template_directory_uri();?>/dist/images/n-slide-prev.svg" alt="">',
			nextArrow: '<img class=" n-slide-next" src=<?php echo get_template_directory_uri();?>/dist/images/n-slide-next.svg" alt="">',
			arrows: true,
			slidesToShow:3,
			responsive: [
			{
				breakpoint: 1024,
				settings: {
					slidesToShow: 2
				}
			},
			{
				breakpoint: 640,
				settings: {
					slidesToShow: 1,
					adaptiveHeight: true
				}
			}
			]
		});

		jQuery('[href="#infrastructure"]').click(function() {

			carousel.resize();
		});

		// var count_news = carousel.slick( "getSlick" );

		// slider = jQuery( ".slider" ).slider({
		// 	min : 0,
		// 	max : 8,
		// 	slide: function(event, ui) {
		// 		var slick = carousel.slick( "getSlick" );
		// 		goTo = ui.value * (slick.slideCount-1) / 8;
		// 		carousel.slick( "goTo", goTo );
		// 	}
		// });


	});


	////////////  map init /////////

	function initMap(){
		var element = document.getElementById('map');
		var style = 
		[
		{
			"featureType": "all",
			"elementType": "geometry",
			"stylers": [
			{
				"color": "#d3efff"
			}
			]
		},
		{
			"featureType": "all",
			"elementType": "labels.text.fill",
			"stylers": [
			{
				"gamma": 1
			},
			{
				"color": "#76cdff"
			},
			{
				"lightness": 1
			}
			]
		},
		{
			"featureType": "all",
			"elementType": "labels.text.stroke",
			"stylers": [
			{
				"saturation": -31
			},

			{
				"lightness": 0
			},
			{
				"weight": 1
			},
			{
				"gamma": 0.2
			}
			]
		},
		{
			"featureType": "landscape",
			"elementType": "geometry",
			"stylers": [
			{
				"lightness": 30
			},
			{
				"saturation": 30
			}
			]
		},
		{
			"featureType": "poi",
			"stylers": [
			{ "visibility": "off" }
			]   
		},
		{
			"featureType": "poi",
			"elementType": "geometry",
			"stylers": [
			{
				"saturation": 20
			}
			]
		},
		{
			"featureType": "poi.park",
			"elementType": "geometry",
			"stylers": [
			{
				"lightness": 20
			},
			{
				"saturation": -20
			}
			]
		},
		{
			"featureType": "road",
			"elementType": "geometry",
			"stylers": [
			{
				"lightness": 10
			},
			{
				"color": "#ffffff"
			},
			{
				"saturation": -30
			}
			]
		},
		{
			"featureType": "road",
			"elementType": "geometry.stroke",
			"stylers": [
			{
				"saturation": 5
			},
			{
				"color": "#cccccc"
			},

			{
				"lightness": 45
			}
			]
		},
		{
			"featureType": "water",
			"elementType": "all",
			"stylers": [
			{
				"lightness": -20
			}
			]
		}
		]

		<?php $main_position=get_field('map_position',pll_current_language('slug'));  ?>
		var options = {
			zoom: 15,
			styles:style,
			center:{
				lat:<?= $main_position['lat'];?>,
				lng:<?= $main_position['lng'];?>

			}
		}; 

		var myMap = new google.maps.Map(element, options);

		var markers = [

		<?php if( have_rows('map_items',pll_current_language('slug')) ) : ?>
		<?php $first_element=true;?>
		<?php while ( have_rows('map_items',pll_current_language('slug')) ) : ?>
		<?php the_row(); ?>

		<?php $position=get_sub_field('position');?>
		{
			coordinates:{
				lat:<?= $position['lat'];?>,
				lng:<?= $position['lng'];?>
			},
			image:{
				url: '<?php the_sub_field('icon');?>',
				<?php if (!$first_element) : ?>
				scaledSize: new google.maps.Size(60, 60),
				origin: new google.maps.Point(0, 0),
				anchor: new google.maps.Point(32,85),
				labelOrigin:  new google.maps.Point(30,70),
			<?php endif; ?>

		},
		label:'<?php echo addslashes(get_sub_field('title')) ;?>',
	},

	<?php $first_element=false;?>

<?php  endwhile; ?>
<?php endif; ?>
];



function addMarker(properties){
	var marker = new google.maps.Marker({
		position: properties.coordinates,
		map: myMap,
		icon:properties.image,
		label:properties.label,
		scaledSize: new google.maps.Size(80, 80),
		origin: new google.maps.Point(0, 0),
		anchor: new google.maps.Point(32,65),
		labelOrigin:  new google.maps.Point(40,75),
	});

	if(properties.image){
		marker.setIcon(properties.image);
	}
	if(properties.info){
		marker.addListener('click',function(){
			InfoWindow.open(myMap, marker);
		});
		var InfoWindow = new google.maps.InfoWindow({
			content:properties.info
		});
	}
}
for(var i = 0; i < markers.length; i++) {
	addMarker(markers[i]);
}
}

</script>
