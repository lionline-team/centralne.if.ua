
<?php while (have_posts()) : the_post(); ?>

 <?php if( have_rows('intro_slider') ):?>

  <section class="main-carousel">
    <div class="main-slider">
      <?php while ( have_rows('intro_slider') ) : ?>
        <?php the_row(); ?>

        <div class="central-slider">
          <img src="<?php the_sub_field('image');?>" alt="">
          <div class="img-wrap"></div>
          <div class="slide-info">
            <h1 class="title"><?php the_sub_field('title');?></h1>
            <h2 class="info-title"><?php the_sub_field('subtitle');?></h2>
            <div class="title-btn">
              <?php $links_count=get_sub_field('links'); ?>
              <?php  if( have_rows('links') ): ?>
                <div class="title-btn">

                  <?php if (count($links_count)==1) $button_classes = array('btn-all ','btn-all');
                  else  $button_classes = array('btn-left btn-main','btn-right btn-main');  ?>
                  <?php $i=0;?>
                  <?php while( have_rows('links') ): the_row(); ?>
                    <?php $button=get_sub_field('link');?>
                    <?php if ($button) : ?>
                      <a class="btn <?= $button_classes[$i];?> scrol"  href="<?= $button['url'];?>"><?= $button['title'];?></a>
                    <?php endif; ?>
                    <?php $i++;?>
                  <?php endwhile; ?>
                  <div class="clearfix"></div>
                </div>
              <?php endif; ?>

              <div class="clearfix"></div>
            </div>
          </div>
        </div>
      <?php  endwhile; ?>
    </div>
    <img class="arrow_bottom arrow-b slider_bottom" src="<?php echo get_template_directory_uri();?>/dist/images/arrow_bottom.svg" alt="">
    <img class="wave main-wave" src="<?php echo get_template_directory_uri();?>/dist/images/main-wave.svg" alt="">
  </section>
<?php endif; ?>

<?php get_template_part( 'templates/block', 'tabs' );?>


<section class="apartments" id="apr">
  <div class="row">
    <h2 class="info-title over">
      <?php the_field('flats_title');?>
    </h2>
    <p class="ap-descr"><?php the_field('flat_subtitle');?></p>
  </div>
  <div class="row">

    <?php if( have_rows('flats_list') ):?>
      <?php while ( have_rows('flats_list') ) : ?>
        <?php the_row(); ?>
        <div class="columns  medium-4">
         <a href="<?php the_sub_field('link');?>" >
          <div class="app-item " >
            <img src="<?php the_sub_field('image');?>" alt="">
            <div class="ap-count"><?php the_sub_field('title');?></div>
          </div>
        </a>
      </div>
    <?php  endwhile; ?>
  <?php endif; ?>

</div>
</section>

<?php endwhile; ?>


<section class="about_dev " >
  <div class="row">
    <img src="<?php echo get_template_directory_uri();?>/dist/images/dev_group.svg" class="dev-group" alt="">
    <h2 class="info-title over"><?php the_field('developer_title',pll_current_language('slug'));?></h2>
    <?php the_field('developer_text',pll_current_language('slug'));?>
    <?php if( have_rows('developer_stats',pll_current_language('slug')) ):?>
     <section class="dev-advantages">
      <?php while ( have_rows('developer_stats',pll_current_language('slug')) ) : ?>
        <?php the_row(); ?>
        <div class="advantage">
          <div class="count"><?php the_sub_field('metric');?></div>
          <p><?php the_sub_field('text');?></p>
        </div>
      <?php  endwhile; ?>
    </section>
  <?php endif; ?>
</div>
</section>


<section class="news" id="news">
  <div class="row">
    <h2 class="info-title over"><?php the_field('one_news_title3',pll_current_language('slug'));  ?></h2>
    <div class="carousel-news">




      <!-- get last posts exclude current -->
      <?php $args = array( 
        'posts_per_page'  => 5,
        );
        $posts = get_posts( $args ); ?>
        <?php
        foreach ( $posts as $post ) : setup_postdata( $post ); ?>

        <?php 
        if (has_post_thumbnail( $post->ID ) ) {
          $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); 
          $src=$image[0]; 
        }
        else {
          $src=get_template_directory_uri().'/dist/images/noimage.png'; 
        }
        ?>


        <div class="news-item">
          <a href="<?php the_permalink();?>">
            <img src="<?php echo $src; ?>" alt="">
          </a>
          <div class="news-info">
            <a href="<?php the_permalink();?>">
              <h3 class="news-title"><?php the_title();?></h3>
            </a>
            <div class="sub-info">
              <span class="left"><?php the_date('d.m.y');?></span>
              <span class="right">Новини</span>
            </div>
            <div class="clearfix"></div>

          </div>
        </div>

      <?php endforeach; 
      wp_reset_postdata();?>


    </div>
  </div>
</section> 
