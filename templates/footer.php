<section class="contacts" id="cont">
	<div class="row">

    <div class="floor_widget">
      <div class="floor_widget__phone">
        <?php $phone= get_field('contact_mainphone',pll_current_language('slug'));  ?>

        <a href="<?= $phone['url']?>">
          <!-- <img src="dist/images/viber_w.svg" alt=""> -->
          <?= $phone['title']?>
          <img src="<?php echo get_template_directory_uri();?>/dist/images/viber_w2.svg" alt="">
        </a>
      </div>
      <div class="floor_widget__icon">
        <a href="<?= $phone['url']?>">
          <img src="<?php echo get_template_directory_uri();?>/dist/images/widget_icon2.svg" alt="">
        </a>
      </div>
    </div>


    <div class="columns medium-6 small-12 contact_us">

     <div class="form-block">
      <div class="contact_us__success">
       <h2 class="info-title"><?php _e('Дякуємо','lionline');?></h2>
       <p><?php _e('Ми отримали ваші дані, наш менеджер зв’яжеться з Вами','lionline');?></p>
       <button id="btn_success" class="submit btn"><?php _e('ок','lionline');?></button>
     </div>
     <h2 class="info-title"><?php _e('Зв’яжіться з нами','lionline');?></h2>
     <p><?php _e('Вкажіть ваші контактні дані, після чого наш менеджер
     зв’яжеться з вами протягом 15хв','lionline');?>
   </p>

   <form class="contact-fom">
     <div class="form-group">
      <label for="name">Ім'я:</label>
      <input class="contactname" type="text" name="name" id="name" placeholder="<?php _e('Ваше Ім`я','lionline');?>">
    </div>
    <div class="form-group">
      <label for="phone">Тел:</label>
      <input class="contactphone"  type="text" name="phone" id="phone" placeholder="<?php _e('+380','lionline');?>" required>
    </div>
    <button type="submit" value="Зв'язатися" class="submit button success"><?php _e('Зв`язатися','lionline');?></button>
  </form>
</div>

</div>

<?php if( have_rows('contact_managers',pll_current_language('slug')) ):?>
 <div class="columns medium-6 small-12 managers">
  <?php while ( have_rows('contact_managers',pll_current_language('slug')) ) : ?>
   <?php the_row(); ?>
   <div class="manager" style="margin-right: 0px;">
    <div class="m-info" style="padding-bottom: 20px;">
     <div class="m-name" ><?php the_sub_field('name');?></div>
     <span><?php the_sub_field('subtitle');?></span>

     <div class="m-phone">
      <?php $contact=get_sub_field('contact');?>
      <a href="<?= $contact['url'];?>"><?= $contact['title'];?></a>
    </div>
    <span class="m-line"></span>
  </div>
  <img src="<?php the_sub_field('photo');?>" alt="" style=" max-width:250px;">
</div>
<?php  endwhile; ?>
</div>
<?php endif; ?>
</div>
<img src="<?php echo get_template_directory_uri();?>/dist/images/footer-wave.svg" alt="" class="wave">
</section>

<footer>
	<section class="footer-menu">
		<div class="row">
			<?php \Roots\Sage\Extras\display_menu_with_title('footer1_navigation'); ?>
			<?php \Roots\Sage\Extras\display_menu_with_title('footer2_navigation'); ?>
			<?php \Roots\Sage\Extras\display_menu_with_title('footer3_navigation'); ?>
			<?php \Roots\Sage\Extras\display_menu_with_title('footer4_navigation'); ?>
			<?php \Roots\Sage\Extras\display_menu_with_title('footer5_navigation'); ?>
		</div>
	</section>
	<section class="sub-footer">
		<a href=""><img src="<?php echo get_template_directory_uri();?>/dist/images/main-logo.svg" alt=""></a>
		<div class="copyright right"> <?php the_field('contacts_footer_textline',pll_current_language('slug'));  ?></div>
	</section>
</footer>


<!-- form action -->
<script>
	if ( typeof contact_form_hook_defined === 'undefined') { // No dublicate hooks, when use two forms on one page




		jQuery(document).on('submit','form.contact-fom',function(e){
			var name = jQuery(this).find('.contactname').val();
			var phone = jQuery(this).find('.contactphone').val();
			var link=<?php echo get_the_ID();?>;


			jQuery(this).find('button').addClass('animate');
			jQuery(this).trigger("reset");
			setTimeout(function(){
				jQuery('.button').removeClass('animate');
				jQuery(".contact_us__success").addClass('active');
			},1000);
			setTimeout(function(){
				jQuery(".contact_us__success").removeClass('active');
			},6000);

			jQuery("#btn_success").click(function(){
				jQuery(".contact_us__success").removeClass('active');
			})


			// /			console.log(name+phone);

			jQuery.ajax({
				url: ajaxurl,
				data: {
					'action':'sendmsg',
					'name':name,
					'phone':phone,
					'link':link,
				},
				success:function(data) {





					jQuery(".contact_us__success").removeClass('active');


					console.log('sended!');
					console.log(data);

				},
				error: function(errorThrown){
					console.log(errorThrown);
				}
			});
			e.preventDefault(e);
		});
		contact_form_hook_defined=true;

	}
</script>
