<?php
/**
 * Template Name: Contacts Template
 */
?>

<?php while (have_posts()) : the_post(); ?>
	<section class="contactPage-contacts" >
		<div class="wrapper clearfix">
			<div class="contactPage__info columns large-6">
				<div class="info-title over">
					<span>
						<?php the_title();?>
					</span>
				</div>
				<div class="contactPage__phone">
					<p><?php _e('Офіс продаж:','lionline');?></p>
					<?php $phone= get_field('contact_mainphone',pll_current_language('slug'));  ?>
					<a href="<?= $phone['url']?>"><?= $phone['title']?></a>
				</div>
				<div class="contactPage__offices">
					<div class="office-item">
						<div class="office-item__title">
							<?php 
							$field=get_field('contacts_block1_title');
							$lines = explode("\n", $field);
							if ( !empty($lines) ) {
								foreach ( $lines as $line ) {
									echo '<span>'. trim( $line ) .'</span>';
								}
							}
							?>
							<?php $button= get_field('contacts_block1_link');  ?>
								<a target="_blank" href="<?= $button['url']; ?>"><?= $button['title']; ?></a>
						</div>
						<div class="office-item__work-time">
							<ul>
								<?php 
								$field=get_field('contacts_block1_text');
								$lines = explode("\n", $field);
								if ( !empty($lines) ) {
									foreach ( $lines as $line ) {
										echo '<li>'. trim( $line ) .'</li>';
									}
								}
								?>
							</ul>
						</div>
					</div>
					<div class="office-item">
						<div class="office-item__title">
							<?php 
							$field=get_field('contacts_block2_title');
							$lines = explode("\n", $field);
							if ( !empty($lines) ) {
								foreach ( $lines as $line ) {
									echo '<span>'. trim( $line ) .'</span>';
								}
							}
							?>
							<?php $button= get_field('contacts_block2_link');  ?>
							<a target="_blank" href="<?= $button['url']; ?>"><?= $button['title']; ?></a>
						</div>
						<div class="office-item__work-time">
							<ul>
								<?php 
								$field=get_field('contacts_block2_text');
								$lines = explode("\n", $field);
								if ( !empty($lines) ) {
									foreach ( $lines as $line ) {
										echo '<li>'. trim( $line ) .'</li>';
									}
								}
								?>
							</ul>
						</div>
					</div>
				</div>
				<div class="contactPage__btn">
					<?php $button=get_field('contacts_button_1');?>
					<a href="<?= $button['url']?>" class="batton scrol"><?= $button['title']?></a>
				</div>
			</div>
			<div class="contactPage__img columns large-6">
				<img src="<?php the_field('contacts_image');?>" alt="">
			</div>
		</div>
	</section>
	<section>
		<div class="contact-map">
			<div id="map">
				<script  defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyApj8qSzyO4DqXeQtFQPKI4mi6p-MBwlpo&amp;callback=initMap"></script>
			</div>
		</div>
		<div class="contact-map__btn">
			<?php $button=get_field('contacts_button_2');?>
			<a href="<?= $button['url']?>" class="batton batton_light"><?= $button['title']?></a>
		</div>
	</section>
<?php endwhile; ?>



<script type="text/javascript">
	jQuery(document).ready(function() {


		jQuery(".slider_bottom").click(function() {
			jQuery('html, body').animate({
				scrollTop: jQuery("#tab-info").offset().top
			}, 1000);
		});
	});


	////////////  map init /////////

	function initMap(){
		var element = document.getElementById('map');
		var style = 
		[
		{
			"featureType": "all",
			"elementType": "geometry",
			"stylers": [
			{
				"color": "#d3efff"
			}
			]
		},
		{
			"featureType": "all",
			"elementType": "labels.text.fill",
			"stylers": [
			{
				"gamma": 1
			},
			{
				"color": "#76cdff"
			},
			{
				"lightness": 1
			}
			]
		},
		{
			"featureType": "all",
			"elementType": "labels.text.stroke",
			"stylers": [
			{
				"saturation": -31
			},

			{
				"lightness": 0
			},
			{
				"weight": 1
			},
			{
				"gamma": 0.2
			}
			]
		},
		// {
			//     "featureType": "all",
			//     "elementType": "labels.icon",
			//     "stylers": [
			//         {
				//             // "visibility": "off"
				//         }
				//     ]
				// },
				{
					"featureType": "landscape",
					"elementType": "geometry",
					"stylers": [
					{
						"lightness": 30
					},
					{
						"saturation": 30
					}
					]
				},
				{
					"featureType": "poi",
					"elementType": "geometry",
					"stylers": [
					{
						"saturation": 20
					}
					]
				},
				{
					"featureType": "poi.park",
					"elementType": "geometry",
					"stylers": [
					{
						"lightness": 20
					},
					{
						"saturation": -20
					}
					]
				},
				{
					"featureType": "road",
					"elementType": "geometry",
					"stylers": [
					{
						"lightness": 10
					},
					{
						"color": "#ffffff"
					},
					{
						"saturation": -30
					}
					]
				},
				{
					"featureType": "road",
					"elementType": "geometry.stroke",
					"stylers": [
					{
						"saturation": 5
					},
					{
						"color": "#cccccc"
					},

					{
						"lightness": 45
					}
					]
				},
				{
					"featureType": "water",
					"elementType": "all",
					"stylers": [
					{
						"lightness": -20
					}
					]
				},
				{
					"featureType": "poi",
					"stylers": [
					{ "visibility": "off" }
					]   
				}
				];

				<?php $main_position=get_field('map_position',pll_current_language('slug'));  ?>
				var options = {
					zoom: 15,
					styles:style,
					center:{
						lat:<?= $main_position['lat'];?>,
						lng:<?= $main_position['lng'];?>

					}
				}; 

				var myMap = new google.maps.Map(element, options);


				var markers = [

				// {
					// 	coordinates:{
						// 		lat:48.916276,
						// 		lng:24.713359
						// 	},

						// 	image:"<?php echo get_template_directory_uri();?>/dist/images/map_logo.png",
						// },

						<?php if( have_rows('map_items',pll_current_language('slug')) ) : ?>
						<?php $first_element=true;?>
						<?php while ( have_rows('map_items',pll_current_language('slug')) ) : ?>
						<?php the_row(); ?>
						<?php if ($first_element) : ?>
						<?php $position=get_sub_field('position');?>
						{
							coordinates:{
								lat:<?= $position['lat'];?>,
								lng:<?= $position['lng'];?>
							},
							image:{
								url: '<?php the_sub_field('icon');?>',
							},
							label:'<?php echo addslashes(get_sub_field('title')) ;?>',
						},

						<?php $first_element=false;?>
					<?php endif; ?>
				<?php  endwhile; ?>
			<?php endif; ?>

			];



			function addMarker(properties){
				var marker = new google.maps.Marker({
					position: properties.coordinates,
					map: myMap,
					icon:properties.image,
					label:properties.label,
					scaledSize: new google.maps.Size(80, 80),
					origin: new google.maps.Point(0, 0),
					anchor: new google.maps.Point(32,65),
					labelOrigin:  new google.maps.Point(40,75),
				});

				if(properties.image){
					marker.setIcon(properties.image);
				}
				if(properties.info){
					marker.addListener('click',function(){
						InfoWindow.open(myMap, marker);
					});
					var InfoWindow = new google.maps.InfoWindow({
						content:properties.info
					});
				}
			}
			for(var i = 0; i < markers.length; i++){
				addMarker(markers[i]);
			}
		}



	</script>
