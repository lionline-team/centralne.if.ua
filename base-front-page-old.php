<?php

use Roots\Sage\Setup;
use Roots\Sage\Wrapper;

?>

<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<?php get_template_part('templates/head'); ?>
<body <?php body_class(); ?>>
  <!--[if IE]>
  <div class="alert alert-warning">
    <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'sage'); ?>
  </div>
  <![endif]-->
  <?php
  do_action('get_header');
      // get_template_part('templates/header');
  ?>

  <header>
    <div class="logo">
      <img src="<?php echo get_template_directory_uri();?>/dist/images/logo.svg" alt="">
    </div>
  </header>
  <div class="container">
    <div class="column-50 first">
      <div class="info">
        <h1>Сайт знаходиться в розробці</h1>
        <p>Наші контакти</p>
        <h3 class="main-phone">
          <span class="icon"><img src="<?php echo get_template_directory_uri();?>/dist/images/phone.svg" alt=""></span>
          +38 050 08 77 777
        </h3>
        <h3 class="main-phone viber">
          <span class="icon"><img src="<?php echo get_template_directory_uri();?>/dist/images/viber.svg" alt=""></span>
          +38 050 08 77 777
        </h3>
        <section class="locations">
          <div class="column-50">
            <h5>
              <span class="icon"><img src="<?php echo get_template_directory_uri();?>/dist/images/location.svg" alt=""></span>
              ЖК “Містечко Центральне”
              вул С.Височана 18
            </h5>
          </div>
          <div class="column-50">
            <h5>
              <span class="icon"><img src="<?php echo get_template_directory_uri();?>/dist/images/location.svg" alt=""></span>Спілка забудівників:
              вул. А.Мельника 10
            </h5>
          </div>
          <div class="clearfix"></div>
        </section>
        <h5 class="other-contacts">
          <span class="icon"><img src="<?php echo get_template_directory_uri();?>/dist/images/small-phone.svg" alt=""></span>+38 050 08 77 777
        </h5>
        <h5 class="other-contacts">
          <span class="icon"><img src="<?php echo get_template_directory_uri();?>/dist/images/site.svg" alt=""></span>www.fil-bud.if.ua
        </h5>
      </div>
    </div>
    <div class="column-50 build">
      <section class="build-proc">
        <div class="cord"></div>
        <img src="<?php echo get_template_directory_uri();?>/dist/images/kran.svg" alt="" class="main">
        <img src="<?php echo get_template_directory_uri();?>/dist/images/loop.svg" alt="" class="loop">
        <img src="<?php echo get_template_directory_uri();?>/dist/images/brick.svg" alt="" class="brick">
      </section>
    </div>
  </div>



  <?php
  do_action('get_footer');
      // get_template_part('templates/footer');
  wp_footer();
  ?>
</body>
</html>
